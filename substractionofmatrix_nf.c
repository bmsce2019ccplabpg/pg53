#include <stdio.h>

int main()
{
    int i,j,r1,c1,r2,c2,a[10][10],b[10][10],d[10][10];
    printf("enter number of rows and columns of first matrix:\n");
    scanf("%d%d",&r1,&c1);
    printf("enter number of rows and columns of second matrix:\n");
    scanf("%d%d",&r2,&c2);
    if(r1!=r2||c1!=c2)
    {
    printf("substraction not possible\n");
    exit(0);
    }
    printf("enter elements of first matrix:\n");
    for(i=0;i<r1;i++)
    {
        for(j=0;j<c1;j++)
        {
            printf("\t a[%d][%d]=",i,j);
            scanf("%d",&a[i][j]);
        }
        printf("\n");
    }
    printf("enter elements of second matrix:\n");
    for(i=0;i<r2;i++)
    {
        for(j=0;j<c2;j++)
        {
            printf("\t b[%d][%d]=",i,j);
            scanf("%d",&b[i][j]);
        }
        printf("\n");
    }
    printf("resuitant matrix after substraction is :\n");
    for(i=0;i<r1;i++)
    {
        for(j=0;j<c1;j++)
        {
            d[i][j]=a[i][j]-b[i][j];
            printf("\t c[%d][%d]=%d",i,j,d[i][j]);
        }
        printf("\n");
    }
    return 0;
}
