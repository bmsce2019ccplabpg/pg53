#include <stdio.h>

int main()
{
  int a[10],n,i,key,l,h,flag=0,m,pos=0;
  printf("how many elements\n");
  scanf("%d",&n);
  printf("enter %d numbers:\n",n);
  for(i=0;i<n;i++)
  {
      scanf("%d",&a[i]);
  }
  printf("enter a key element:\n");
  scanf("%d",&key);
  l=0,h=n-1;
  while(l<=h)
  {
      m=(l+h)/2;
      if(a[m]==key)
      {
          flag=1;
          pos=m+1;
          printf("element %d found at %d position\n",key,pos);
          break;
      }
      if(key>a[m])
      {
          l=m+1;
      }
      if(key<a[m])
      {
          h=m-1;
      }
  }
  return 0;
}