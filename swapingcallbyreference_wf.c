#include <stdio.h>
int swap(int *,int *);
int main()
{
    int x,y;
    printf("enter x value:\t x=");
    scanf("%d",&x);
    printf("enter y value:\t y=");
    scanf("%d",&y);
    swap(&x,&y);
    printf("after swaping x=%d  and y=%d",x,y);
    return 0;
}
int swap(int *x,int *y)
{
    int t;
    t=*x;
    *x=*y;
    *y=t; 
}
