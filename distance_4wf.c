#include <stdio.h>
#include<math.h>
int input();
float dist();
void output(float);
int main()
{
    int x1,y1,x2,y2;
    float d;
    x1=input();
    y1=input();
    x2=input();
    y2=input();
    d=dist(x1,y1,x2,y2);
    output(d);
    return 0;
}
int input()
{
    int a;
    printf("enter co ordinate of points:\n");
    scanf("%d",&a);
    return a;
}
float dist(int x1,int y1,int x2,int y2)
{
    float d;
    d=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    return d;
}
void output(float d)
{
    printf("distance between two points=%f",d);
}