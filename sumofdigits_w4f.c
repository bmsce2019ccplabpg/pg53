#include <stdio.h>
#include<math.h>
int input();
int sum();
void output();
int main()
{
    int n,s;
    n=input();
    s=sum(n);
    output(s);
    return 0;
}
int input()
{
    int a;
    printf("enter any number:\t");
    scanf("%d",&a);
    return a;
}
int sum(int n)
{
    int s=0,r;
    while(n!=0)
    {
        r=n%10;
        s+=r;
        n/=10;
    }
    return s;
}
void output(int s)
{
    printf("sum of its digits=%d",s);
}